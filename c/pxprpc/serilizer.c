
#include <pxprpc.h>
#include <stdlib.h>

static void _pxprpc_ensure_buf(struct pxprpc_serilizer *ser,uint32_t remain){
    if(ser->cap-ser->pos<remain){
        int newsize=ser->pos+remain;
        newsize+=newsize>>1;
        ser->buf=pxprpc__realloc(ser->buf,newsize);
    }
}

extern void pxprpc_ser_prepare_ser(struct pxprpc_serilizer *ser,int initBufSize){
    ser->buf=pxprpc__malloc(initBufSize);
    ser->cap=initBufSize;
    ser->pos=0;
}
extern void pxprpc_ser_prepare_unser(struct pxprpc_serilizer *ser,void *buf,uint32_t size){
    ser->cap=size;
    ser->pos=0;
    ser->buf=buf;
}

extern void pxprpc_ser_put_int(struct pxprpc_serilizer *ser,uint32_t val){
    _pxprpc_ensure_buf(ser,4);
    *(uint32_t *)(ser->buf+ser->pos)=val;
    ser->pos+=4;
}
extern void pxprpc_ser_put_long(struct pxprpc_serilizer *ser,uint64_t val){
    _pxprpc_ensure_buf(ser,8);
    *(uint64_t *)(ser->buf+ser->pos)=val;
    ser->pos+=8;
}
extern void pxprpc_ser_put_float(struct pxprpc_serilizer *ser,float val){
    _pxprpc_ensure_buf(ser,4);
    *(float *)(ser->buf+ser->pos)=val;
    ser->pos+=4;
}
extern void pxprpc_ser_put_double(struct pxprpc_serilizer *ser,double val){
    _pxprpc_ensure_buf(ser,8);
    *(double *)(ser->buf+ser->pos)=val;
    ser->pos+=8;
}
extern void pxprpc_ser_put_bytes(struct pxprpc_serilizer *ser,struct pxprpc_bytes *val){
    if(val->length<=0xff){
        _pxprpc_ensure_buf(ser,1+val->length);
        *(ser->buf+ser->pos)=val->length;
        ser->pos++;
    }else{
        _pxprpc_ensure_buf(ser,5+val->length);
        *(ser->buf+ser->pos)=0xff;
        ser->pos++;
        *(uint32_t *)(ser->buf+ser->pos)=val->length;
        ser->pos+=4;
    }
    memcpy(ser->buf+ser->pos,&val->data,val->length);
    ser->pos+=val->length;
}
extern uint32_t pxprpc_ser_get_int(struct pxprpc_serilizer *ser){
    uint32_t val=*(uint32_t *)ser->buf+ser->pos;
    ser->pos+=4;
    return val;
}
extern uint64_t pxprpc_ser_get_long(struct pxprpc_serilizer *ser){
    uint64_t val=*(uint64_t *)ser->buf+ser->pos;
    ser->pos+=8;
    return val;
}
extern float pxprpc_ser_get_float(struct pxprpc_serilizer *ser){
    float val=*(float *)ser->buf+ser->pos;
    ser->pos+=4;
    return val;
}
extern double pxprpc_ser_get_double(struct pxprpc_serilizer *ser){
    double val=*(double *)ser->buf+ser->pos;
    ser->pos+=8;
    return val;
}
extern struct pxprpc_bytes *pxprpc_ser_get_bytes(struct pxprpc_serilizer *ser){
    int len=*(ser->buf+ser->pos);
    ser->pos++;
    if(len==0xff){
        len=*(uint32_t *)(ser->buf+ser->pos);
        ser->pos+=4;
    }
    struct pxprpc_bytes *val=(struct pxprpc_bytes *)pxprpc__malloc(4+len);

    memcpy(&val->data,ser->buf+ser->pos,len);
    ser->pos+=val->length;
    return val;
}